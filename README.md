# LiveScore challenge

### Final answer
Please considering the plot below.

![plot03](plot03.png "Cumulative sums of acquired places in channels competition")

    A  =  UDP 10.10.10.4:33000 > 239.22.0.4:51000
    B  =  UDP 10.10.10.1:33000 > 239.22.0.1:51000
    C  =  UDP 10.10.10.3:33000 > 239.22.0.3:51000
    D  =  UDP 10.10.10.2:33000 > 239.22.0.2:51000

The plot shows that in longer term channels B and D are the last to deliver up-to-date stock information.

My final answer is - keep channels A and C.

### Work outline
I did analysis in Jupyter notebook. The work can be inspected and reproduced using [results_martins_mednis.ipynb](results_martins_mednis.ipynb)  notebook.
Prior running the notebook, I recommend to setup _virtual environment_ with required python packages installed. A short instruction on that can be found at the end of this Readme.

I also provide an exported notebook with pre-rendered plots: [results_martins_mednis.html](results_martins_mednis.html)

### Setting up the virtual environment

```bash
which python3 #Output: /usr/bin/python3
mkvirtualenv --python=/usr/bin/python3 acs
pip install -r requirements.txt

cd livescore_challenge
jupyter notebook
```

